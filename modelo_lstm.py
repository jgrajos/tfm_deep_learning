#!/usr/bin/env python
# -*- coding: utf-8 -*- 

##################################################
import logging
logger = logging.getLogger('jmlog')

import numpy as np
import tensorflow as tf
# from keras import backend
# from keras.models import Sequential
# from keras.layers import Dense
# from keras.layers import LSTM


class LSTMModel(object):
	def __init__(self, n_features, n_hidden_layers=2, batch_size=1):
		self.n_features = n_features
		self.n_hidden_layers = n_hidden_layers
		self.batch_size = batch_size

		# Reseteamos el grafo
		tf.reset_default_graph()

		# #creamos la LSTM network
		# backend.clear_session() #keras.backend.clear_session()
		model = tf.keras.models.Sequential()
		#Capas ocultas

		# model.add(tf.keras.layers.LSTM(n_hidden_layers, batch_input_shape=(batch_size, n_features, 1)))l
		# model.add(tf.keras.layers.LSTM(n_hidden_layers, batch_input_shape=(batch_size, n_features, 1), stateful=True))

		# model.add(tf.keras.layers.LSTM(n_hidden_layers, input_shape=(n_features, 1), stateful=True))
		model.add(LSTM(4, batch_input_shape=(batch_size, time_steps, features), stateful=True))

		#Capa de salida
		model.add(tf.keras.layers.Dense(1))
		model.compile(loss='mean_squared_error', optimizer='adam')

		self.model = model

	def get_label(self):
		return '{m} ({n}|{h}|{b})'.format(n=self.n_features, h=self.n_hidden_layers, m=self.__class__.__name__, b=self.batch_size)

	def data_reshape_in(self, data_x):
		# remodelamos la entrada para que sea[muestras, pasos de tiempo, características]
		return np.reshape(data_x, (data_x.shape[0], data_x.shape[1], 1))

	def data_reshape_out(self, data_predict):
		return data_predict[:,0]

	def train(self, x_data, y_data, epochs=1):
		model = self.model
		n_features = self.n_features
		batch_size = self.batch_size

		#Ajustamos datos para este modelo
		# x_data = x_data.reshape(-1, 1, n_features)
		x_data = self.data_reshape_in(x_data)

		history = model.fit(x_data, y_data, epochs=epochs, batch_size=batch_size, verbose=0, shuffle=False)
		# model.reset_states()



	def predict(self, x_data):
		n_features = self.n_features
		model = self.model

		#Ajustamos datos para este modelo
		x_data = self.data_reshape_in(x_data)

		# hacemos las predicciones
		y_predict = model.predict(x_data)
		return self.data_reshape_out(y_predict)

