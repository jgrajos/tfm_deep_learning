#!/usr/bin/env python
# -*- coding: utf-8 -*- 

##################################################
import logging
logger = logging.getLogger('jmlog')

import matplotlib.pyplot as plt

def graficar_data(figure, detail, data_y, y_epoch):
	
	if not plt.fignum_exists(figure):
		plt.figure(figure)
		plt.title(figure)

		plt.plot(y_epoch, data_y, 'g-', label='data_y')

def graficar_prediccion(figure, detail, train_predict, train_epoch,
					 validation_predict, validation_epoch, test_predict, test_epoch):
	
	plt.figure(figure)
	plt.title(figure)

	p = plt.plot(train_epoch, train_predict, label='train_predict: '+detail)
	color = p[0].get_color()
	
	plt.plot(validation_epoch, validation_predict, color=color, linestyle=':', label='validation_predict: '+detail)

	plt.plot(test_epoch, test_predict, color=color, linestyle='--', label='test_predict: '+detail)

	plt.legend(loc='upper left', bbox_to_anchor= (1.01, 1.0), fontsize='xx-small')
	plt.tight_layout(pad=1)


def graficar_data_prediccion(y_data, y_epoch, y_train_predicted, y_train_epoch,
					 y_test_predicted, y_test_epoch, title):
	plt.figure()
	plt.title(title)
	# plt.subplot(1, 2, 1)
	plt.plot(y_epoch, y_data, 'b-', label='y_data')
	plt.legend()
	
	# plt.subplot(1, 2, 2)
	plt.plot(y_train_epoch, y_train_predicted, 'r-', label='y_train_predicted')
	plt.legend()

	plt.plot(y_test_epoch, y_test_predicted, 'g-', label='y_test_predicted')
	plt.legend()

	# plt.draw()
	# plt.show(block=False)

def graficar_prediccion_mse(figure, detail, y_data, y_epoch, y_train_predicted, y_train_epoch,
					 y_test_predicted, y_test_epoch, mse_train, mse_test):
	plt.figure(detail)
	plt.title(detail)

	plt.plot(y_epoch, y_data, 'b-', label='y_data')
	plt.legend()
	
	plt.plot(y_train_epoch, y_train_predicted, 'r-', label='y_train_predicted')
	plt.legend()

	plt.plot(y_test_epoch, y_test_predicted, 'g-', label='y_test_predicted')
	plt.legend()

	plt.plot(y_train_epoch, mse_train, '', label='mse_train')
	plt.legend(fontsize = 'x-small')

	plt.plot(y_test_epoch, mse_test, '', label='mse_test')
	plt.legend(fontsize = 'x-small')

def graficar_mse(figure, detail, train_mse, validation_mse):
	plt.figure(figure)
	plt.title(figure)
	
	plt.plot(train_mse, label=detail+': train_mse')
	plt.plot(validation_mse, label=detail+': validation_mse')
	
	plt.legend(loc='upper left', bbox_to_anchor= (1.01, 1.0), fontsize='xx-small')
	plt.tight_layout(pad=1)

