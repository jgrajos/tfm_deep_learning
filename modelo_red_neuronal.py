#!/usr/bin/env python
# -*- coding: utf-8 -*- 

##################################################
import logging
logger = logging.getLogger('jmlog')


import pandas as pd
import numpy as np

import tensorflow as tf



class RedNeuronalModel(object):
   def __init__(self):
	  self.net = None
	  self.out = None
	  self.X = None

   def calculate(self, x_data, y_data, STEPS=8, LEARNING_RATE=0.5):
	  n_features = x_data.shape[1]
	  n_stocks = n_features

	  # Neurons
	  n_neurons_1 = 1024
	  n_neurons_2 = 512
	  n_neurons_3 = 256
	  n_neurons_4 = 128

	  # Session
	  net = tf.InteractiveSession()

	  # Placeholder
	  X = tf.placeholder(dtype=tf.float32, shape=[None, n_stocks])
	  self.X = X
	  Y = tf.placeholder(dtype=tf.float32, shape=[None])

	  # Initializers
	  sigma = 1
	  weight_initializer = tf.variance_scaling_initializer(mode="fan_avg", distribution="uniform", scale=sigma)
	  bias_initializer = tf.zeros_initializer()

	  # Hidden weights
	  W_hidden_1 = tf.Variable(weight_initializer([n_stocks, n_neurons_1]))
	  bias_hidden_1 = tf.Variable(bias_initializer([n_neurons_1]))
	  W_hidden_2 = tf.Variable(weight_initializer([n_neurons_1, n_neurons_2]))
	  bias_hidden_2 = tf.Variable(bias_initializer([n_neurons_2]))
	  W_hidden_3 = tf.Variable(weight_initializer([n_neurons_2, n_neurons_3]))
	  bias_hidden_3 = tf.Variable(bias_initializer([n_neurons_3]))
	  W_hidden_4 = tf.Variable(weight_initializer([n_neurons_3, n_neurons_4]))
	  bias_hidden_4 = tf.Variable(bias_initializer([n_neurons_4]))

	  # Output weights
	  W_out = tf.Variable(weight_initializer([n_neurons_4, 1]))
	  bias_out = tf.Variable(bias_initializer([1]))

	  # Hidden layer
	  hidden_1 = tf.nn.relu(tf.add(tf.matmul(X, W_hidden_1), bias_hidden_1))
	  hidden_2 = tf.nn.relu(tf.add(tf.matmul(hidden_1, W_hidden_2), bias_hidden_2))
	  hidden_3 = tf.nn.relu(tf.add(tf.matmul(hidden_2, W_hidden_3), bias_hidden_3))
	  hidden_4 = tf.nn.relu(tf.add(tf.matmul(hidden_3, W_hidden_4), bias_hidden_4))

	  # Output layer (transpose!)
	  out = tf.transpose(tf.add(tf.matmul(hidden_4, W_out), bias_out))
	  self.out = out
	  # Cost function
	  mse = tf.reduce_mean(tf.squared_difference(out, Y))

	  # Optimizer
	  opt = tf.train.AdamOptimizer().minimize(mse)

	  # Init
	  net.run(tf.global_variables_initializer())

	  for step in xrange(STEPS):

		 # Shuffle training data
		 shuffle_indices = np.random.permutation(np.arange(len(y_data)))
		 X_train = x_data[shuffle_indices]
		 y_train = y_data[shuffle_indices]

		 net.run(opt, feed_dict={X: X_train, Y: y_train})

	  self.net = net

   def predict(self, x_data):
	  net = self.net
	  X = self.X
	  out = self.out
	  y_predicted = net.run(out, feed_dict={X: x_data})
	  return y_predicted.transpose()

