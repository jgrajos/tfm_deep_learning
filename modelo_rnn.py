#!/usr/bin/env python
# -*- coding: utf-8 -*- 

##################################################
import logging
logger = logging.getLogger('jmlog')

import numpy as np
import tensorflow as tf



class RNNModel(object):
	def __init__(self, n_features, n_hidden_layers=2, batch_size=1):

		# Reseteamos el grafo
		tf.reset_default_graph()


		self.n_features = n_features
		self.n_hidden_layers = n_hidden_layers
		self.batch_size = batch_size

		# Number of stocks in training data
		n_stocks = n_features #X_train.shape[1]

		# Neurons
		n_neurons_1 = 1024
		n_neurons_2 = 512
		n_neurons_3 = 256
		n_neurons_4 = 128

		# Session
		net = tf.InteractiveSession()

		# Placeholder
		X = tf.placeholder(dtype=tf.float32, shape=[None, n_stocks])
		Y = tf.placeholder(dtype=tf.float32, shape=[None])

		# Initializers
		sigma = 1
		weight_initializer = tf.variance_scaling_initializer(mode="fan_avg", distribution="uniform", scale=sigma)
		bias_initializer = tf.zeros_initializer()

		# Hidden weights
		W_hidden_1 = tf.Variable(weight_initializer([n_stocks, n_neurons_1]))
		bias_hidden_1 = tf.Variable(bias_initializer([n_neurons_1]))
		W_hidden_2 = tf.Variable(weight_initializer([n_neurons_1, n_neurons_2]))
		bias_hidden_2 = tf.Variable(bias_initializer([n_neurons_2]))
		W_hidden_3 = tf.Variable(weight_initializer([n_neurons_2, n_neurons_3]))
		bias_hidden_3 = tf.Variable(bias_initializer([n_neurons_3]))
		W_hidden_4 = tf.Variable(weight_initializer([n_neurons_3, n_neurons_4]))
		bias_hidden_4 = tf.Variable(bias_initializer([n_neurons_4]))

		# Output weights
		W_out = tf.Variable(weight_initializer([n_neurons_4, 1]))
		bias_out = tf.Variable(bias_initializer([1]))

		# Hidden layer
		hidden_1 = tf.nn.relu(tf.add(tf.matmul(X, W_hidden_1), bias_hidden_1))
		hidden_2 = tf.nn.relu(tf.add(tf.matmul(hidden_1, W_hidden_2), bias_hidden_2))
		hidden_3 = tf.nn.relu(tf.add(tf.matmul(hidden_2, W_hidden_3), bias_hidden_3))
		hidden_4 = tf.nn.relu(tf.add(tf.matmul(hidden_3, W_hidden_4), bias_hidden_4))

		# Output layer (transpose!)
		out = tf.transpose(tf.add(tf.matmul(hidden_4, W_out), bias_out))

		# Cost function
		mse = tf.reduce_mean(tf.squared_difference(out, Y))

		# Optimizer
		opt = tf.train.AdamOptimizer().minimize(mse)

		# Init
		net.run(tf.global_variables_initializer())

		self.net = net
		self.opt = opt
		self.X = X
		self.Y = Y
		self.out = out

	def get_label(self):
		return '{m} ({n}|{h}|{b})'.format(n=self.n_features, h=self.n_hidden_layers, m=self.__class__.__name__, b=self.batch_size)

	def data_reshape_in(self, data_x):
		return data_x

	def data_reshape_out(self, data_predict):
		return data_predict[0,:]

	def train(self, x_data, y_data, epochs=1):
		batch_size = self.batch_size
		net = self.net
		opt = self.opt
		X = self.X
		Y = self.Y

		# # Shuffle training data
		# shuffle_indices = np.random.permutation(np.arange(len(y_data)))
		# x_data = x_data[shuffle_indices]
		# y_data = y_data[shuffle_indices]

		# Minibatch training
		for i in range(0, len(y_data) // batch_size):
			start = i * batch_size
			batch_x = x_data[start:start + batch_size]
			batch_y = y_data[start:start + batch_size]
			
			# Run optimizer with batch
			net.run(opt, feed_dict={X: batch_x, Y: batch_y})


	def predict(self, x_data):
		net = self.net
		out = self.out
		X = self.X

		pred = net.run(out, feed_dict={X: x_data})
		# import pdb; pdb.set_trace()
		return self.data_reshape_out(pred)
