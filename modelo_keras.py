#!/usr/bin/env python
# -*- coding: utf-8 -*- 

##################################################
import logging
logger = logging.getLogger('jmlog')

import numpy as np
import tensorflow as tf
# from keras import backend
# from keras.models import Sequential
# from keras.layers import Dense
# from keras.layers import LSTM


class LSTMModelKeras(object):
	def __init__(self, n_features, n_hidden_layers=2):
		self.n_features = n_features
		self.n_hidden_layers = n_hidden_layers

	def get_label(self):
		return '{m} ({n}|{h})'.format(n=self.n_features, h=self.n_hidden_layers, m=self.__class__.__name__)

	def train(self, x_data, y_data, steps=8, learning_rate=0.5):

		# Obtenemos numero de caracteristicas
		n_features = self.n_features # x_data.shape[1]
		n_hidden_layers = self.n_hidden_layers

		#Ajustamos datos para este modelo
		x_data = x_data.reshape(-1, 1, n_features)

		# #creamos la LSTM network
		# backend.clear_session() #keras.backend.clear_session()
		model = tf.keras.models.Sequential()
		#Capas ocultas
		model.add(tf.keras.layers.LSTM(n_hidden_layers, input_shape=(1, n_features)))
		#Capa de salida
		model.add(tf.keras.layers.Dense(1))
		model.compile(loss='mean_squared_error', optimizer='adam')
		model.fit(x_data, y_data, epochs=steps, batch_size=10, verbose=0)

		self.model = model


	def predict(self, x_data):

		n_features = self.n_features
		model = self.model

		#Ajustamos datos para este modelo
		x_data = x_data.reshape(-1,1,n_features)

		# hacemos las predicciones
		y_predict = model.predict(x_data)
		return y_predict

