#!/usr/bin/env python
# -*- coding: utf-8 -*- 

##################################################
import logging
logger = logging.getLogger('jmlog')

import numpy as np
import tensorflow as tf



class RegresionLinealModel(object):
	def __init__(self, n_features, learning_rate=0.05):
		self.n_features = n_features
		self.learning_rate = learning_rate

		# Reseteamos el grafo
		tf.reset_default_graph()

		# Normalizamos tipos de datos
		dtype = 'float32'

		# Obtenemos numero de caracteristicas
		n_features = self.n_features #x_data.shape[1]

		# Diseñamos el modelo
		# y = x * W + b
		x_playholder = tf.placeholder(dtype)
		y_playholder = tf.placeholder(dtype)


		W_playholder = tf.Variable(tf.zeros(shape=[n_features,1], dtype=dtype), dtype=dtype, name="W")
		b_playholder = tf.Variable(tf.zeros(shape=[], dtype=dtype), dtype=dtype, name="b")

		# Output
		output = tf.add(tf.matmul(x_playholder, W_playholder), b_playholder)

		loss = tf.reduce_mean(tf.square(output - y_playholder))
		optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)
		# accuracy = optimizer.minimize(loss)
		
		init = tf.global_variables_initializer()
		
		# with tf.Session() as sess:
		sess = tf.Session()
		sess.run(init)

		self.session = sess
		self.optimizer = optimizer
		# self.accuracy = accuracy
		self.x_playholder = x_playholder
		self.y_playholder = y_playholder
		self.output = output
		# self.W_playholder = W_playholder
		# self.b_playholder = b_playholder


	def get_label(self):
		return '{m} ({n}|{rl})'.format(n=self.n_features,rl=self.learning_rate,  m=self.__class__.__name__)

	def train(self, x_data, y_data, epochs=8, batch_size=0):
		sess = self.session
		optimizer = self.optimizer
		x_playholder = self.x_playholder
		y_playholder = self.y_playholder

		x_data = x_data
		y_data = y_data.reshape([-1,1])

		# x_data = np.array([
		# 						[1,1,1],
		# 						[2,2,2],
		# 						[3,3,3],
		# 	]).astype('float64')

		# y_data = np.array([
		# 						1,
		# 						2,
		# 						3,
		# ]).astype('float64')
		

		# for epoch in xrange(epochs):
		sess.run(optimizer, feed_dict={x_playholder: x_data, y_playholder:y_data})


	def predict(self, x_data):
		sess = self.session
		x_playholder = self.x_playholder
		output = self.output

		# x_data = np.reshape(x_data, (x_data.shape[0], x_data.shape[1], 1))
		y_predicted = sess.run(output, feed_dict={x_playholder: x_data})
		return y_predicted[:,0]


