#!/usr/bin/env python
# -*- coding: utf-8 -*- 

##################################################
import logging
logger = logging.getLogger('jmlog')

import os
import datetime

import cPickle as pickle

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf

from sklearn import preprocessing
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler


# Clases para escalar los datos
class Scaler_MinMax():
	def __init__(self):
		self.scaler = MinMaxScaler(feature_range=(0,1))

	def transform(self, dataset):
		return self.scaler.fit_transform(dataset)

	def inverse(self, dataset_scaled):
		dataset_scaled = dataset_scaled.reshape(-1,1)
		return self.scaler.inverse_transform(dataset_scaled)

class Scaler_Standard():
	def __init__(self):
		self.scaler = StandardScaler()

	def transform(self, dataset):
		return self.scaler.fit_transform(dataset)

	def inverse(self, dataset_scaled):		
		dataset_scaled = dataset_scaled.reshape(-1,1)
		return self.scaler.inverse_transform(dataset_scaled)

class Scaler_prof():
	def __init__(self):
		pass

	def transform(self, dataset):
		return preprocessing.scale(dataset)

	def inverse(self, dataset_scaled):
		pass

class Scaler_jm():
	def __init__(self):
		self.mean = None
		self.std = None

	def transform(self, dataset):
		if type(self.mean) == type(None):
			self.mean = dataset.mean()
			self.std = dataset.std()

		return (dataset-self.mean)/self.std

	def inverse(self, dataset_scaled):
		return (dataset_scaled * self.std) + self.mean


class Scaler(Scaler_Standard):
	pass


def get_df_features(data, n_features=1):
	data = data[:,0]
	df = pd.DataFrame({})
	for i in range(n_features):
		df['x{}'.format(i)] = data[i:len(data)-n_features+i]
	df['y'] = data[n_features:len(data)]
	df['y_epoch'] = np.arange(n_features,len(data))
	return df

def _get_df_features(data, n_features=1):
	df = pd.DataFrame({})
	for i in range(n_features):
		df['x{}'.format(i)] = data[i:len(data)-n_features+i]
	df['y'] = data[n_features:len(data)]
	df['y_epoch'] = np.arange(n_features,len(data))
	return df

def scale(df, mean=None, std=None):
	 if type(mean) == type(None):
		mean = df.mean()
		std = df.std()
	 df_scale = (df - mean) / std
	 return df_scale, mean, std

def scale_reverse(df_scale, mean, std):
	df = (df_scale * std) + mean
	return df


#Funcion para generar un dataframe a partir del fichero
def load_file(directory, filename):
	 filename_abs_path = directory+filename
	 
	 parse = lambda x: datetime.datetime.fromtimestamp(float(x)/1000)
	 logger.debug('Cargando fichero: {}'.format(filename_abs_path))
	 data_frame = pd.read_csv(filename_abs_path, sep='\t', encoding="utf-8-sig", names=['CellID', 'datetime', 'countrycode', 'smsin', 'smsout', 'callin', 'callout', 'internet'], parse_dates=['datetime'], date_parser=parse)

	 logger.debug('Fichero cargado: {}'.format(filename))
	 return data_frame

#Funcion para cargar datos proporcionados en el articulo nature
def get_data_nature(date_start, date_num_days, type_prediction, data_dir, file_template):
	 df = pd.DataFrame({})
	 for index in range(date_num_days):
		date = datetime.datetime.strptime(date_start, "%Y-%m-%d").date()+datetime.timedelta(days=index)
		date_str = date.strftime("%Y-%m-%d")
		
		df2 = load_file(data_dir, file_template.format(date_str))
		df2 = df2.groupby(['datetime'], as_index=False).sum()
		df2 = df2.set_index('datetime')
		df = df.append(df2)

	 return df[type_prediction].values


def get_data(date_start, date_num_days, type_prediction, data_dir, file_template, cache=True):
	logger.debug('Cargando datos')
	df = pd.DataFrame({})
	for index in range(date_num_days):
		date = datetime.datetime.strptime(date_start, "%Y-%m-%d").date()+datetime.timedelta(days=index)
		date_str = date.strftime("%Y-%m-%d")
		filename = file_template.format(date_str)
		filename_cache = 'cache/'+filename+'.pickle'

		# Cargamos cache
		df2 = get_data_cache(filename_cache, cache)
		if type(df2)==type(None):
			# Cargamos datos y agrupamos
			df2 = load_file(data_dir, filename)
			df2 = df2.groupby(['datetime'], as_index=False).sum()
			df2 = df2.set_index('datetime')

			# Guardamos datos en cache
			set_df_cache(df2, filename_cache)

		df = df.append(df2)

	return df[type_prediction].values


#Funcion para cargar un dataframe con opcion de cache
def _get_data(date_start, date_num_days, type_prediction, data_dir, file_template, cache=True):
	 logger.debug('Cargando datos')
	 filename_cache = 'cache/nature_{}_{}_{}.pickle'.format(date_start, date_num_days, type_prediction)
	 data = get_data_cache(filename_cache, cache)
	 if type(data)==type(None):
		data = get_data_nature(date_start, date_num_days, type_prediction, data_dir, file_template)
		set_df_cache(data, filename_cache)
	 logger.debug('Datos cargados')
	 return data

#Funcion para cargar	datos de cache
def get_data_cache(filename, cache):
	 data = None
	 if os.path.isfile(filename) and cache:
		logger.debug('Cargando pickle: {}'.format(filename))
		data = pickle.load( open( filename, "rb" ) )
	 return data

#Funcion para cachear datos en un fichero
def set_df_cache(data, filename):
	 logger.debug('Guardando pickle: {}'.format(filename))
	 pickle.dump( data, open( filename, "wb" ) )


def get_mse(data, predict):
	n = len(data)
	mse = np.sum((predict-data)**2)/n
	return mse

def get_mse_relativo(data, predict):
	no_zero_index = np.where(data!=0)
	data = data[no_zero_index]
	predict = predict[no_zero_index]
	n = len(data)
	# mse = np.mean(((predict-data)/data)**2)
	mse = np.sum(((predict-data)/data)**2)/n
	return mse

def get_mse_array(y_data, y_data_epoch, y_predicted, y_predicted_epoch):
	 se_accumulated = 0
	 mse_array = []
	 # se_array= []
	 for i in range(len(y_predicted)):
		epoch_predicted = y_predicted_epoch[i]
		value_predicted = y_predicted[i]
		value_real = y_data[y_data_epoch.tolist().index(epoch_predicted)]

		se = (value_predicted - value_real)**2
		se_accumulated += se
		mse = (se_accumulated)/(i+1)
		
		mse_array.append(mse)
		# se_array.append(se)
	 return mse_array

