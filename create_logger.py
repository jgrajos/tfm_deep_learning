#!/usr/bin/env python
# -*- coding: utf-8 -*- 

##################################################
import sys
import logging

# create logger
# Logger root
logger_root = logging.getLogger('jmlog') #logging.getLogger('__main__')
logger_root.setLevel(logging.DEBUG)

# # Logger nodo clase
# logger_nodo_clase = logging.getLogger('nodo_clase')
# logger_nodo_clase.setLevel(logging.DEBUG)

# # Logger __name__
# logger = logging.getLogger(__name__)
# logger.setLevel(logging.DEBUG)

# Create Handlers
# create console handler and set level to debug
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)

# Create formatter
formatter = logging.Formatter('[+] %(asctime)s | %(filename)s:%(lineno)d | %(name)s | %(levelname)s - %(message)s')

# Add formatter to handler
handler.setFormatter(formatter)

# Add handler to logger
logger_root.addHandler(handler)

