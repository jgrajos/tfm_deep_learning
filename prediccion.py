#!/usr/bin/env python
# -*- coding: utf-8 -*- 

##################################################
import logging
logger = logging.getLogger('jmlog')

import create_logger

import traceback

import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt

from sklearn import preprocessing
from sklearn.preprocessing import MinMaxScaler

import datetime

from data import get_data, get_df_features, Scaler, Scaler_Standard, Scaler_MinMax, scale, scale_reverse, get_mse, get_mse_relativo, get_mse_array
from graficar import graficar_data, graficar_prediccion, graficar_data_prediccion, graficar_mse, graficar_prediccion_mse

# Modelos de prediccion
from modelo_regresion_multiple import RegresionLinealModel
from modelo_rnn import RNNModel
from modelo_lstm import LSTMModel



def run_prediction(type_data, data, n_features, model_prediction, epochs=50, scaler_class=Scaler, **kwargs_model):
	'''
		Metodo run_prediction:

		type_data
		# label para el array de datos (data)

		data
		# array de tamaño (n,1) con los valores reales

		n_features
		# número de valores anteriores a utilizar en la prediccion
		# n_features = 3, X0, X1, X2 -> Y

		model_prediction
		# Clase del modelo de prediccion a utilizar
		
		#No hay retorno
		return None

	'''
	try:
		logger.debug('Datos {td}, modelo {m}'.format(td=type_data, m=model_prediction.__name__))
		logger.debug('{nf} features ({kw})'.format(nf=n_features, kw=kwargs_model))


		# data es un array simple
		# transformamos para conseguir array de arrays de un elemento.de

		dataset = pd.DataFrame(data).values.astype('float32')

		# Escalamos los datos
		scaler = scaler_class()
		dataset_scaled = scaler.transform(dataset)


		dataframe = get_df_features(dataset_scaled, n_features=n_features)
		data_y = dataframe['y'].values
		data_n = len(data_y)
		data_epoch = dataframe['y_epoch'].values
		data_x = dataframe.drop(columns=['y', 'y_epoch']).values

		# Partimos los datos en 3 conjutnos.
		# Train, Validation y Test		

		train_rate = 0.6
		train_n =int(data_n*train_rate)
		train_index = range(train_n)
		train_x = data_x[train_index]
		train_y = data_y[train_index]

		validation_rate = 0.2
		validation_n =int(data_n*validation_rate)
		validation_index = range(train_n,train_n+validation_n)
		validation_x = data_x[validation_index]
		validation_y = data_y[validation_index]

		
		test_index = range(train_n+validation_n,data_n)
		test_x = data_x[test_index]
		test_y = data_y[test_index]

		#Obtenemos los datos de salida sin escalar
		data_y_unscaled = dataset[data_epoch]
		train_y_unscaled = dataset[data_epoch[train_index]]
		validation_y_unscaled = dataset[data_epoch[validation_index]]
		test_y_unscaled = dataset[data_epoch[test_index]]

		#Instanciamos modelo de prediccion
		model = model_prediction(n_features, **kwargs_model)

		train_mse = []
		train_mse_relativo = []
		validation_mse = []
		validation_mse_relativo = []

		d1 = datetime.datetime.now()

		for epoch in range(epochs):

			d2 = datetime.datetime.now()
			if d2-d1 > datetime.timedelta(seconds=10):
				d1 = datetime.datetime.now()
				logger.debug("Seguimos entrenando -> {}/{}".format(epoch+1, epochs))

			# Entrenamos
			model.train(train_x, train_y)
			
			# Prediccciones
			# Con datos de entrenamiento
			train_predict = model.predict(train_x)
			train_predict_inverse = scaler.inverse(train_predict)

			train_mse.append( get_mse(train_y_unscaled, train_predict_inverse) )
			train_mse_relativo.append( get_mse_relativo(train_y_unscaled, train_predict_inverse) )
			
			# Con datos de validacion
			validation_predict = model.predict(validation_x)
			validation_predict_inverse = scaler.inverse(validation_predict)

			validation_mse.append( get_mse(validation_y_unscaled, validation_predict_inverse) )
			validation_mse_relativo.append( get_mse_relativo(validation_y_unscaled, validation_predict_inverse) )

		
		test_predict = model.predict(test_x)
		test_predict_inverse = scaler.inverse(test_predict)

		test_mse = get_mse(test_y_unscaled, test_predict_inverse)
		test_mse_relativo = get_mse_relativo(test_y_unscaled, test_predict_inverse)

		
		# Invertimos el escalado
		data_y = data_y_unscaled
		train_predict = train_predict_inverse
		validation_predict = validation_predict_inverse
		test_predict = test_predict_inverse


		# get_mse_relativo(dataset[data_epoch[train_index]] ,scaler.inverse(train_predict.reshape(-1,1)))

		logger.debug('Resultado del entrenamiento: {0:.6f} MSE, {1:.6f} MSE Relativo'.format(train_mse[-1], train_mse_relativo[-1]))
		logger.debug('Resultado de la validacion: {0:.6f} MSE, {1:.6f} MSE Relativo'.format(validation_mse[-1], validation_mse_relativo[-1]))
		logger.debug('Resultado del test: {0:.6f} MSE, {1:.6f} MSE Relativo'.format(test_mse, test_mse_relativo))


		# Preparamos graficas
		detail_figure = model.get_label()
		
		# Imprimimos todas las predicciones en una grafica
		graficar_data('DATA {}'.format(type_data), detail_figure, data_y, data_epoch)
		graficar_prediccion('DATA {}'.format(type_data), detail_figure, train_predict, data_epoch[train_index], validation_predict, data_epoch[validation_index], test_predict, data_epoch[test_index])
		
		# Imprimimos MSE
		graficar_mse('MSE {}'.format(type_data), detail_figure, train_mse, validation_mse)
		graficar_mse('MSE Relativo {}'.format(type_data), detail_figure, train_mse_relativo, validation_mse_relativo)


	except Exception,e:
		logger.error("{te}: {e}\n{tb}".format(te=type(e).__name__,e=e,tb=traceback.format_exc()))
		


DATA_DIR = '/media/sf_tfm/'
DATA_FILE_template = 'sms-call-internet-mi-{}.txt'
DATE_START = '2013-11-04'
DATE_NUM_DAYS = 14#(3+1+1)*7
TYPE_PREDICTION = 'callout' #['CellID', 'datetime', 'countrycode', 'smsin', 'smsout', 'callin', 'callout', 'internet']


if __name__ == '__main__':
	data = get_data(DATE_START, DATE_NUM_DAYS, TYPE_PREDICTION, DATA_DIR, DATA_FILE_template)

	# for x in [1,3,5,7,9]:
	# prediction_model = RegresionLinealModel
	# epochs = 200
	# n_features = 5
	# kwargs_model = {}
	# kwargs_model['learning_rate'] = 0.1
	# run_prediction(TYPE_PREDICTION, data, n_features, prediction_model, epochs=epochs, **kwargs_model)

	# # for x in [256,1024,2048]:
	prediction_model = RNNModel
	scaler = Scaler_MinMax
	epochs = 200
	n_features = 5
	kwargs_model = {}
	kwargs_model['batch_size'] = 256
	run_prediction(TYPE_PREDICTION, data, n_features, prediction_model, epochs=epochs, scaler_class=scaler, **kwargs_model)

	# for x in [32]:
	# 	prediction_model = LSTMModel
	# 	n_features = 3
	# 	kwargs_model = {}
	# 	kwargs_model['batch_size'] = x
	# 	kwargs_model['n_hidden_layers'] = 6
	# 	run_prediction(TYPE_PREDICTION, data, n_features, prediction_model, **kwargs_model)

	
	
	#Wait to not close figures
	try:
		plt.show(block=False)
		raw_input("\nPress the enter to close.")
	except:
		pass